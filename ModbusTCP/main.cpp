#include "basemodbus.h"
#include "readcoil.h"
#include "holdingregister.h"
#include "discreteinput.h"
#include "inputregister.h"
#include <memory>

int main()
{
	boost::asio::io_service service;
	boost::asio::ip::tcp::endpoint ep502(boost::asio::ip::address_v4::from_string("127.0.0.1"), 502);	
	std::unique_ptr<ModbusInstance> modbus(new ModbusInstance(service));
	modbus->connect(ep502); 	

	ReadCoil* pReadCoil = new ReadCoil(modbus.get());
	HoldingRegister* pHoldingRegister = new HoldingRegister(modbus.get());
	pHoldingRegister->write();	
	pReadCoil->write();		
	

	service.run();	
	
	std::system("pause");
	return 0;
}