#pragma once

#include "basemodbus.h"
#include <bitset>

class ReadCoil : public BaseModbus
{
public:
	ReadCoil(std::shared_ptr<ModbusInstance> modbus);
	~ReadCoil();
	void write() ;
	void writeHandler(const boost::system::error_code& e, size_t byte);
	void read() ;
	void readHandler(const boost::system::error_code& e, size_t byte) ;


	std::vector<bool> result;
	uint8_t respone[1024];
private:
	void timeout(const boost::system::error_code&e);
	void wait();
	uint8_t frame[12];	
	short startAddr, num_val;
};

