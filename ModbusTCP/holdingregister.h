#pragma once
#include "basemodbus.h"


class HoldingRegister : public BaseModbus
{
public:
	HoldingRegister(std::shared_ptr<ModbusInstance> modbus);
	~HoldingRegister();
	 void write() ;
	 void writeHandler(const boost::system::error_code& e, size_t byte) ;
	 void read() ;
	 void readHandler(const boost::system::error_code& e, size_t byte);	
	
	std::vector<int> repVector;
	
private:	
	void timeout(const boost::system::error_code&e);
	void wait();

	uint8_t frame[12];
	uint8_t respone[1024];
	short startAddr, num_val;
	
};