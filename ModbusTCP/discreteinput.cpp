#include "discreteinput.h"

DiscreteInput::DiscreteInput(std::shared_ptr<ModbusInstance> modbus): BaseModbus(modbus)
{
	std::cout << "Function code 05, writecoil." << std::endl;
	memset(frame, 0, 12);

	

}

DiscreteInput::~DiscreteInput()
{
}

void DiscreteInput::write()
{
	std::cout << "Input coil to write: ";
	std::cin >> startAddr;
	std::cin.ignore();
	std::cout << "Value to write: 0 or 255: ";
	std::cin >> num_val;
	std::cin.ignore();

	frame[0] = 0;
	frame[1] = 3;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 5; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val);
	frame[11] = (uint8_t)(num_val >> 8);

	sock->async_write_some(boost::asio::buffer(frame, 12), boost::bind(&DiscreteInput::writeHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void DiscreteInput::writeHandler(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		read();
	}
	else
	{
		sock->close();
	}
}

void DiscreteInput::read()
{

	memset(respone, 0, 1024);
	sock->async_read_some(boost::asio::buffer(respone, 1024), boost::bind(&DiscreteInput::readHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void DiscreteInput::readHandler(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		
		if (respone[10] == 255)
		{
			std::cout << "Write to coil : True" << std::endl;
		}
		if (respone[10] == 0)
		{
			std::cout << "Write to coil : False" << std::endl;
		}
		
	}
	else
	{
		std::cout << "Discrete Input not completed!" << std::endl;
	}
}
