#include "readcoil.h"

ReadCoil::ReadCoil(std::shared_ptr<ModbusInstance> modbus) : BaseModbus(modbus)
{
	std::cout << "Function code 01" << std::endl;
	memset(frame, 0, 12);
	startAddr = 0;
	num_val = 6;
	frame[0] = 0;
	frame[1] = 1;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 1; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
}

ReadCoil::~ReadCoil()
{

}

void ReadCoil::write()
{
	sock->async_write_some(boost::asio::buffer(frame, 12), boost::bind(&ReadCoil::writeHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void ReadCoil::writeHandler(const boost::system::error_code & e, size_t byte)
{

	if (!e)
	{
		read();
	}
	else
	{
		sock->close();
	}
}

void ReadCoil::read()
{		
	memset(respone, 0, 1024);
	sock->async_read_some(boost::asio::buffer(respone, 1024), [this](const boost::system::error_code & e, size_t byte)
	{	
		if (!e)
		{
			if (respone[1]==1)			
				readHandler(e, byte);
			return;
		}
		else
		{
			std::cout << "Reading Error!" << std::endl;
			return;
		}
	});	
	wait();
}

void ReadCoil::readHandler(const boost::system::error_code & e, size_t byte)
{	
	{
		std::vector<bool>temp;
			temp = result;
			result.clear();
			std::bitset<6> t(respone[9]);
			for (int i = 0; i < 5; i++)
			{
				result.push_back(t[i]);
			}		
			if (temp == result)
			{
			}
			if (temp != result)
			{
				std::vector<bool>::iterator i;
				for (i=result.begin(); i != result.end(); i++)
				{
					std::cout << std::boolalpha << *i << " ";
				}
				std::cout << std::endl;
			}			
		}		
	}	

void ReadCoil::timeout(const boost::system::error_code & e)
{
	if (e)
		return;
	write();
}

void ReadCoil::wait()
{
	t->expires_from_now(boost::posix_time::seconds(2)); 
	t->async_wait(boost::bind(&ReadCoil::timeout, this, boost::asio::placeholders::error));
}

