#pragma once
#include "basemodbus.h"

class InputRegister : public BaseModbus
{
public:
	InputRegister(ModbusInstance* modbus);
	virtual ~InputRegister();
	 void write();
	 void writeHandler(const boost::system::error_code& e, size_t byte) ;
	void read();
	 void readHandler(const boost::system::error_code& e, size_t byte) ;
private:
	uint8_t frame[12];
	uint8_t respone[1024];
	short startAddr, num_val;
};