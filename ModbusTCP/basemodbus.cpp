#include "basemodbus.h"




BaseModbus::BaseModbus(std::shared_ptr<ModbusInstance> instance) : work(instance->_work), sock(&(instance->_sock)), t(&(instance->_t))
{
	
}

BaseModbus::~BaseModbus()
{
	sock ->close();
	//service.stop();
}

ModbusInstance::ModbusInstance(boost::asio::io_service& _service): _sock(_service), _work(_service), _t(_service)
{
}

ModbusInstance::~ModbusInstance()
{
}

void ModbusInstance::connect(boost::asio::ip::tcp::endpoint & ep)
{
	
	_sock.async_connect(ep, [this](const boost::system::error_code & ec) {
		if (!ec)
		{
			std::cout << "CONNECTED TO MODBUS SLAVE." << std::endl;
		}
		else
		{
			std::cout << "No Connection!" << std::endl;
			
		}

	});
}

