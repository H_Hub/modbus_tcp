#include "holdingregister.h"

HoldingRegister::HoldingRegister(std::shared_ptr<ModbusInstance> modbus) :BaseModbus(modbus)
{
	std::cout << "Function code 03" << std::endl;
	
	memset(frame, 0, 12);
	startAddr = 0;
	num_val = 6;
	frame[0] = 0;
	frame[1] = 3;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 3; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
}

HoldingRegister::~HoldingRegister()
{

}

void HoldingRegister::write()
{	
	sock->async_write_some(boost::asio::buffer(frame, 12), boost::bind(&HoldingRegister::writeHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

}

void HoldingRegister::writeHandler(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		read();
	}
	else
	{
		sock->close();
	}
}

void HoldingRegister::read()
{
	memset(respone, 0, 1024);
	
	sock->async_read_some(boost::asio::buffer(respone, 1024), boost::bind(&HoldingRegister::readHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	wait();
}

void HoldingRegister::readHandler(const boost::system::error_code & e, size_t byte)
{	
	if (!e)
	{
		if (respone[1] == 3)
		{
			int size = (respone[8]);
			std::vector<int> tempVector;
			tempVector = repVector;
			repVector.clear();
			std::vector<int> rep;
		
			for (int j = 9; j <= (size + 8); j++)
			{
				rep.insert(rep.end(), (int16_t)(respone[j]));
				
			}
			int count = 0;
			while (count < size)
			{
				int temp = rep[count] * 256 + (rep[count + 1]);
				repVector.insert(repVector.end(), temp);
				count += 2;
			}
			rep.clear();

			if (tempVector != repVector)
			{
				for (auto k : repVector)
				{
					std::cout << k << " ";
				}
				std::cout << "\n";
			}
			if (tempVector == repVector)
			{
			}
		}
	}
	else
	{
		std::cout << "Reading Register error!\n";
		return;
	}

}

void HoldingRegister::timeout(const boost::system::error_code & e)
{
	if (e)
		return;
	write();
}

void HoldingRegister::wait()
{
	t->expires_from_now(boost::posix_time::seconds(1)); 
	t->async_wait(boost::bind(&HoldingRegister::timeout, this, boost::asio::placeholders::error));
}
