#include "inputregister.h"

InputRegister::InputRegister(ModbusInstance* modbus) :BaseModbus(modbus)
{
	std::cout << "Function code 06, input Reg." << std::endl;
	memset(frame, 0, 12);
	
}

InputRegister::~InputRegister()
{
}

void InputRegister::write()
{
	std::cout << "Input register to write: ";
	std::cin >> startAddr;
	std::cin.ignore();
	std::cout << "Value to write: ";
	std::cin >> num_val;
	std::cin.ignore();

	frame[0] = 0;
	frame[1] = 3;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 6; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
	sock->async_write_some(boost::asio::buffer(frame, 12), boost::bind(&InputRegister::writeHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void InputRegister::writeHandler(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		read();
	}
	else
	{
		sock->close();
	}
}

void InputRegister::read()
{
	memset(respone, 0, 1024);
	sock->async_read_some(boost::asio::buffer(respone, 1024), boost::bind(&InputRegister::readHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}


void InputRegister::readHandler(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		std::cout << "input Register successfully" << std::endl;
	}
	else
	{
		std::cout << "inputReg not completed!" << std::endl;
	}
}
