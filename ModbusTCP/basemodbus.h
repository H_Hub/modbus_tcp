#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdint>
#include <thread>
#include <string>
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <vector> 
#include <mutex>


class ModbusInstance
{
public:
	ModbusInstance(boost::asio::io_service& _service);
	virtual ~ModbusInstance();
	boost::asio::io_service::work _work;
	boost::asio::ip::tcp::socket _sock;
	boost::asio::deadline_timer _t;
	
	void connect(boost::asio::ip::tcp::endpoint& ep);
	
};

class BaseModbus
{
public:
	BaseModbus(std::shared_ptr<ModbusInstance> instance);
	virtual ~BaseModbus();
	
public:
	
	boost::asio::io_service::work& work;
	boost::asio::ip::tcp::socket* sock;
	boost::asio::deadline_timer* t;
	
};